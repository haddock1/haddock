# What is Haddock? 

* A relative to the Cod fish.

* A cranky sea captain in the comic book stories of Tintin.

* The <a href="https://sailfishos.org/develop/hadk/">Sailfish OS HADK</a> as a program.

# What is Haddock NOT?

* The negative thoughts you might have about it.

## Great! How do I install Haddock?

As this idea is very fresh there is NO program ready to install
<b><u>YET!</u></b> and that's were the next question comes natural.

### Why did you start this project without any template to work on?
Good question!
Really!
Well, if you have previously used the Sailfish OS HADK before,
please feel free to push commits to help build this program and make it better.
To help keep this program working and up-to-date you should use the instructions
found in the updated HADK as a reference.

#### Are there any plans to make Haddock run on all operating systems?
The short answer is:
- YES!

##### Contributing
Pull requests are welcome.
For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## License
[GPLv3]

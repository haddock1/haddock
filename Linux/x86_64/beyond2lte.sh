#!/bin/bash

minfo "Making .hadk.env"
cat <<'EOF' > $HOME/.hadk.env
export ANDROID_ROOT="$HOME/hadk"
export VENDOR="samsung"
export DEVICE="beyond2lte"
export PORT_ARCH="aarch64"
alias mb2='mb2 --output-dir "${ANDROID_ROOT?}/droid-local-repo/${DEVICE?}"'
EOF

# Add environment variables for Mer SDK #
cat <<'EOF' >> $HOME/.mersdkubu.profile
function hadk() { source $HOME/.hadk.env; echo "Env setup for $DEVICE"; }
export PS1="HABUILD_SDK [\${DEVICE}] $PS1"
hadk
EOF

echo "Adding PLATFORM_SDK_ROOT=/srv/sailfishos to $HOME/.bashrc"
export PLATFORM_SDK_ROOT=/srv/sailfishos
curl -k -O https://releases.sailfishos.org/sdk/installers/latest/Jolla-latest-SailfishOS_Platform_SDK_Chroot-i486.tar.bz2
sudo mkdir -p $PLATFORM_SDK_ROOT/sdks/sfossdk
sudo tar --numeric-owner -p -xjf Jolla-latest-SailfishOS_Platform_SDK_Chroot-i486.tar.bz2 -C $PLATFORM_SDK_ROOT/sdks/sfossdk
echo "export PLATFORM_SDK_ROOT=$PLATFORM_SDK_ROOT" >> ~/.bashrc
echo 'alias sfossdk=$PLATFORM_SDK_ROOT/sdks/sfossdk/sdk-chroot' >> ~/.bashrc; exec bash
echo 'PS1="PlatformSDK $PS1"' > ~/.mersdk.profile
echo '[ -d /etc/bash_completion.d ] && for i in /etc/bash_completion.d/*;do . $i;done' >> ~/.mersdk.profile
sfossdk

exit

sfossdk

exit

cat <<'EOF' >> $HOME/.mersdk.profile
function hadk() { source $HOME/.hadk.env; echo "Env setup for $DEVICE"; }
hadk
EOF

#Update zypper repos
sudo zypper ref

#Install needed packages
sudo zypper in android-tools-hadk kmod createrepo_c

cat /etc/os-release to check version (Current version is 4.4.0.58 (Vanha Rauma))

TARBALL=ubuntu-focal-20210531-android-rootfs.tar.bz2
curl -O https://releases.sailfishos.org/ubu/$TARBALL
UBUNTU_CHROOT=$PLATFORM_SDK_ROOT/sdks/ubuntu
sudo mkdir -p $UBUNTU_CHROOT
sudo tar --numeric-owner -xjf $TARBALL -C $UBUNTU_CHROOT

#Enter the HABUILD_SDK
ubu-chroot -r $PLATFORM_SDK_ROOT/sdks/ubuntu

mkdir -p $HOME/.bin
PATH="${HOME}/.bin:${PATH}"
curl https://storage.googleapis.com/git-repo-downloads/repo > $HOME/.bin/repo
chmod a+rx $HOME/.bin/repo

#Tell Git who you are#
git config --global user.name "My Name Here"
git config --global user.email "myemailadresshere@company.com"

sudo mkdir -p $ANDROID_ROOT
sudo chown -R $USER $ANDROID_ROOT
cd $ANDROID_ROOT

repo init -u https://github.com/sailinggalaxians/android.git -b hybris-18.1

cd $HOME/hadk/.repo/

mkdir $HOME/hadk/local_manifests

#Add proper repos
cat <<'EOF' >> $HOME/hadk/.repo/local_manifests/$DEVICE.xml
<?xml version="1.0" encoding="UTF-8"?>
  <manifest>
      <project name="LineageOS/android_device_samsung_beyond2lte" path="device/samsung/beyond2lte" />
      <project name="LineageOS/android_device_samsung_exynos9820-common" path="device/samsung/exynos9820-common" remote="github" />
      <project name="sailinggalaxians/android_kernel_samsung_exynos9820" path="kernel/samsung/exynos9820" remote="github" />
      <project name="whatawurst/android_vendor_samsung_beyond2lte" path="vendor/samsung/beyond2lte" remote="github" />
      <project name="LineageOS/android_device_samsung_slsi_sepolicy" path="device/samsung_slsi/sepolicy" remote="github" />
      <project name="LineageOS/android_hardware_samsung" path="hardware/samsung" remote="github" />
      <project name="sailinggalaxians/hybris-boot" path="hybris/hybris-boot" remote="github" />
  </manifest>
EOF

cd $ANDROID_ROOT

repo sync
